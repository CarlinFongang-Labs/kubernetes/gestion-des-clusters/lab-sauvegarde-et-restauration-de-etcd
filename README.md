# Lab : Sauvegarde et restauration d'etcd

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 


# Scénario 

Vous travaillez pour BeeHive, une société de services d'abonnement qui propose des expéditions hebdomadaires d'abeilles aux clients. L'entreprise utilise Kubernetes pour exécuter certaines de ses applications et souhaite s'assurer que son infrastructure Kubernetes est robuste et capable de se remettre des pannes.

Votre tâche consiste à établir un processus de sauvegarde et de restauration pour les données du cluster Kubernetes. Sauvegardez les etcddonnées du cluster, puis restaurez-les pour vérifier que le processus fonctionne.

Vous pouvez trouver des certificats avec lesquels vous authentifier etcddans /home/cloud_user/etcd-certs.

# Objectifs

- Sauvegarder les données etcd

- Restaurer les données etcd à partir de la sauvegarde

# Sauvegarde d'etcd 

## 1. Récupération de la clé valeur 

Nous allons rechercher la valeur "cluster.name" dans notre cluster "etcd"

````bash
ETCDCTL_API=3 etcdctl get cluster.name \
  --endpoints=https://10.0.1.101:2379 \
  --cacert=/home/cloud_user/etcd-certs/etcd-ca.pem \
  --cert=/home/cloud_user/etcd-certs/etcd-server.crt \
  --key=/home/cloud_user/etcd-certs/etcd-server.key
````

>![Alt text](img/image.png)
*La clé valeur est beebox*

## 2. Sauvegarde

````bash
ETCDCTL_API=3 etcdctl snapshot save /home/cloud_user/etcd_backup.db \
  --endpoints=https://10.0.1.101:2379 \
  --cacert=/home/cloud_user/etcd-certs/etcd-ca.pem \
  --cert=/home/cloud_user/etcd-certs/etcd-server.crt \
  --key=/home/cloud_user/etcd-certs/etcd-server.key
````
Cette commande est assez similiaire à la commande de récuparation de la clé valeur, nous passons en option nos trois fichiers de certificat et un endpoints.

La principale différence ici est qu'au lieu de fiare un get sur "cluster.name", nous effectuons plutôt une sauvegarde d'instantané d' etcdctl **"etcdctl snapshot save"**, ceci suivi du nom du fichier dans lequel nous allons stocker notre sauvegarde **"/home/cloud_user/etcd_backup.db"**

>![Alt text](img/image-1.png)
*Création de la sauvegarde d'etcd*

Nous allons ensuite vérifier la création de notre fichier de sauvegarde **"etcd_backup.db"**

````bash
ls
````

>![Alt text](img/image-2.png)
*fichier etcd_backup.db crée*


## Suppression des données du cluster et restauration d'etcd

Nous allons commencé par stoper **"etcd"**

````bash
sudo systemctl stop etcd
````

Une fois **"etcd"** arrêté, nous allons supprimer le repertoire **"sudo rm -rf /var/lib/etcd"**


````bash
sudo rm -rf /var/lib/etcd
````

A présent que toutes les données ont été supprimées, nous allons à présent restaurer ces données à partir de la sauvegarde (**"etcd_backup.db"**) que nous avons précédemnt réalisée

Pour cela, nous allons utiliser **"etcdctl"** que nous allons appliquer sur le fichier de sauvegarde

Il est important de noter que la restauration des instantanés est particulière, nous allons lancer premièrement un cluster **"etcd temporaire"**, afin de reconstruire le repertoire de donnée **"/var/lib/etcd/"** que nous avons précédement supprimé.

A cette commande nous allons rajouter en option la création d'un cluster appelé **"tcd-restore"** dont l'eplacement sera à l'adresse ip privée renseignée, la seconde option **"-initial-advertise-peer-urls"** est importante pour la création d'un cluster temporaire, le nom du noeud sera **"etcd-restore"** et enfin les données seront définies sur le repertoire **"/var/lib/etcd"** en provenance du cluster temporaire

````bash
sudo ETCDCTL_API=3 etcdctl snapshot restore /home/cloud_user/etcd_backup.db \
  --initial-cluster etcd-restore=https://10.0.1.101:2380 \
  --initial-advertise-peer-urls https://10.0.1.101:2380 \
  --name etcd-restore \
  --data-dir /var/lib/etcd
````

>![Alt text](img/image-3.png)
*Restauration d'etcd*

Nous pouvons apprésent afficher le contenu du repertoire **"/var/lib/etcd/"** précédement supprimé

````bash
sudo ls /var/lib/etcd/
````

>![Alt text](img/image-4.png)
*Restauration de /var/lib/etcd*

Changement des droit sur le repertoire afin que l'utilisateur courant y est accès

````bash
sudo chown -R etcd:etcd /var/lib/etcd/
sudo systemctl start etcd
````

Une fois les droits changé, nous allons affichier de nouveau les clé valeur de **"cluster.name"**

````bash
ETCDCTL_API=3 etcdctl get cluster.name \
  --endpoints=https://10.0.1.101:2379 \
  --cacert=/home/cloud_user/etcd-certs/etcd-ca.pem \
  --cert=/home/cloud_user/etcd-certs/etcd-server.crt \
  --key=/home/cloud_user/etcd-certs/etcd-server.key
````

>![Alt text](img/image-5.png)
*Valeur contenu dans etcd restaurée*

Nous avons ainsi ruéssi la restauration d'etcd